# Leeloo LXP Activities and Resources Tracking #

Learning Activities and Resources | Real-Time Tracking

## Description ##

This plugin allows administrators and authorized personnel to track chosen users time spent in Moodle LMS Activities and Resources in Real-Time. It will then sync that data to Leeloo LXP for Learning Analytics, Gamification and Student Relationship Management purposes.

Managers can see and export reports of time spent on specific Activities and Resources, with several filters and templates. All reports are fully customizable, so you can create and interpret this data in the Actionable Learning Analytics module.

The Leeloo LXP Activities and Resources Tracking module is the second engagement booster in the Actionable Learning Analytics and the Advanced Gamification modules.

Use it in tandem with the Leeloo LXP Attendance block!

Your users will see a Real-Time clock with their time spent in the current activity, along with their attendance, their break time and their expected schedule.

This plugin requires the Leeloo LXP Login and Attendance Tracking plugin, the Leeloo LXP Synchronizer plugin and the Leeloo LXP SSO plugin.

Setup

Go to Admin > Plugins > Local plugins > Leeloo LXP Web Activity Tracking

- Add your license key

- Enable the plugin

How it works

• Once the plugin is enabled: whenever users with time tracking activated opens a synced Activity/Resource, the time on that Activity/Resource will be tracked in Real-time in Leeloo LXP
